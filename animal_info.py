class Animal(object):
    pass


class Dog(Animal):
    def __init__(self, name):
        self.name = name
        self.color = None
        self.has_fur = True

    def bark(self):
        print('woof woof!')

    def __str__(self):
        return self.name


class Cat(Animal):
    def __init__(self, name):
        self.name = name
        self.color = None
        self.has_fur = True

    def meow(self):
        print('meow!')


class Poodle(Dog):
    def __init__(self, name):
        super().__init__(name)
        self.has_fur = False


class Person(Animal):
    def __init__(self, name):
        self.name = name
        self.pets = []

    def list_pets(self):
        print([pet.name for pet in self.pets])

    def add_pet(self, pet):
        self.pets.append(pet)
