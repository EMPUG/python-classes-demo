from animal_info import Animal, Dog, Cat, Poodle, Person

fido = Dog('Fido')
my_cat = Cat('Fluffy')
poodle_monster = Poodle('Frunobulax') # See Cheepnis by Frank Zappa
bruce = Person('bruce')
bruce.pets = [fido, my_cat]
